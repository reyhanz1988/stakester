import MasterApi from '../api/MasterApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';

// getLang
export const GET_LANG = createAsyncActionType('GET_LANG');
export const getLang = vars => dispatch => {
    dispatch({
        type: GET_LANG.REQUESTED,
    });
    MasterApi.getLang()
    .then(response => {
        dispatch({
            type: GET_LANG.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_LANG.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// postLang
export const POST_LANG = createAsyncActionType('POST_LANG');
export const postLang = vars => dispatch => {
    dispatch({
        type: POST_LANG.REQUESTED,
        payload: { vars },
    });  
};