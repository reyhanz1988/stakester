import ProductApi from '../api/ProductApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';

// getGames
export const GET_GAMES = createAsyncActionType('GET_GAMES');
export const getGames = vars => dispatch => {
    dispatch({
        type: GET_GAMES.REQUESTED,
    });
    ProductApi.getGames(vars)
    .then(response => {
        dispatch({
            type: GET_GAMES.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_GAMES.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};