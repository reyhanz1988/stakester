import * as React from 'react';
import { Image, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LanguagesScreen from '../screens/LanguagesScreen';
import languages from '../screensTranslations/AppNav';
import {FIRST_COLOR,SECOND_COLOR} from "@env";

const Stack = createStackNavigator();

const HomeNav = () => {
    return(
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen 
                name="Home"
                component={HomeScreen}
            />
        </Stack.Navigator>
    )
}
export {HomeNav}

const SettingsNav = () => {
    return(
        <Stack.Navigator screenOptions={{headerShown: false}}>
            <Stack.Screen 
                name="Settings"
                component={SettingsScreen}
            />
            <Stack.Screen
                name="Languages"
                component={LanguagesScreen}
            />
        </Stack.Navigator>
    )
}
export {SettingsNav}