import * as React from 'react';
import { Image, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {HomeNav, SettingsNav} from './AppStack'
import {FIRST_COLOR,SECOND_COLOR} from "@env";

const Tab = createBottomTabNavigator();

export default class AppNav extends React.Component {
    render() {
        return (
            <Tab.Navigator
                initialRouteName="HomeNav"
                screenOptions={{
                    tabBarActiveTintColor: FIRST_COLOR,
                    tabBarInactiveTintColor: "#666",
                    headerShown: false,
                    tabBarItemStyle: {"backgroundColor": SECOND_COLOR},
                    tabBarStyle: [
                        {"display": "flex"},
                        null
                    ]
                }}
            >
                <Tab.Screen
                    name="HomeNav"
                    component={HomeNav}
                    options={{
                        tabBarLabel: 'Stakester',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="lightning-bolt" color={color} size={26} />
                        ),
                    }}
                />
                <Tab.Screen
                    name="SettingsNav"
                    component={SettingsNav}
                    options={{
                        tabBarLabel: 'Settings',
                        tabBarIcon: ({ color }) => (
                            <MaterialCommunityIcons name="cog" color={color} size={26} />
                        ),
                    }}
                />
            </Tab.Navigator>
        );
    }
}
