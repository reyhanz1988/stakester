module.exports =  [
/*0*/
{
    de: "Heim",
    en: "Home",
    es: "Casa",
    fr: "Accueil",
    it: "Casa",
    nl: "Huis",
    no: "Hjem",
    pt: "Casa",

},

/*1*/
{
    de: "Einstellungen",
    en: "Settings",
    es: "Ajustes",
    fr: "Réglages",
    it: "Impostazioni",
    nl: "Instellingen",
    no: "Innstillinger",
    pt: "Configurações",

},

/*2*/
{
    de: "Wird geladen",
    en: "Loading",
    es: "Cargando",
    fr: "Chargement",
    it: "Caricamento In corso",
    nl: "Bezig Met Laden",
    no: "Laster",
    pt: "Recarregar",

},
];