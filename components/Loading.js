import React, {Component} from 'react';
import { ActivityIndicator, Dimensions, Image, ImageBackground, StyleSheet, Text, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import logo from '../assets/images/logo.png';
import languages from '../screensTranslations/Loading';
class Loading extends Component {
    componentDidMount() {
        AsyncStorage.getItem('LANG').then((value) => {
            if(value){
                this.setState({ currentLang: value });
            }
        });
    }
    constructor() {
        super()
        this.state = {
            currentLang: 'en',
        }
    }
    render() {
        return (
            <ImageBackground source={require('../assets/images/loading.png')} style={{width: '100%', height: '100%'}}  imageStyle= {{}}>
                <View style={styles.loadingWrapper}>
                    <ActivityIndicator style={{marginTop:250}} size='large' />
                </View>
            </ImageBackground>
        )
    }
}
const styles = StyleSheet.create({
    loadingWrapper: {
        flex: 1,
        justifyContent:'center',
        width:'100%',
        height:'100%'
    },
    bgLoading:{
        position: 'absolute',
        bottom: 20,
        right: 20,
        width: Dimensions.get('window').width * 0.4,
        resizeMode:'contain'
    },
})
export default Loading