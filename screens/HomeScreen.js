import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ActivityIndicator, Dimensions, Image, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Appbar, Button, Card, List, Title, Paragraph, Searchbar } from 'react-native-paper';
import logo from '../assets/images/logo.png';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Home';
import {FIRST_COLOR, SECOND_COLOR} from "@env";

class HomeScreen extends React.Component {
    componentDidMount() {
        AsyncStorage.getItem("LANG").then(value => {
        if(value == null){
            AsyncStorage.setItem('LANG', 'en').then((value) => {
                this.setState({currentLang: 'en'}, () =>{
                    this.getData();
                });
            });
        }
        else{
            this.setState({ currentLang: value }, () =>{
                this.getData();
            });
        }});
    }
    componentDidUpdate(prevprops) {
        if(prevprops.getGamesRes !== this.props.getGamesRes){
            if(this.props.getGamesRes == 'error'){
                this.setState({
                    getDataError: this.props.getGamesRes,
                    isLoading: false,
                    refresh: false
                });
            }
            else{
                if(this.props.getGamesRes.status_msg){
                    this.setState({
                        getDataError: this.props.getGamesRes.status_msg,
                        isLoading: false,
                        refresh: false
                    });
                }
                else{
                    if(this.state.currentPage == 1){
                        this.setState({ getDataError: '',getGames: this.props.getGamesRes,isLoading: false, refresh: false });
                    }
                    else{
                        if(this.props.getGamesRes.length > 0){
                            let concatData = [...this.state.getGames, ...this.props.getGamesRes];
                            this.setState({ getGames: concatData }, () => {
                                this.setState({moreLoading: false});
                            });
                        }
                        else{
                            this.setState({moreLoading: false});
                        }
                    }
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            refresh: false,
            moreLoading:false,
            currentLang: 'en',
            getGames: [],
            typingTimeout:false,
            searchText: '',
            currentPage: 1,
            perPage: 10,
        };
        this.getData = this.getData.bind(this);
        this.getMoreData = this.getMoreData.bind(this);
        this.searchGames = this.searchGames.bind(this);
    }
    getData(){
        this.setState({ 
            currentPage: 1,
            refresh: true,
            getGames: []
        }, () =>{
            let vars = this.state;
            this.props.getGames(vars);
        });
    }
    getMoreData(){
        if(this.state.moreLoading == false){
            let currentPage = this.state.currentPage;
            this.setState({ moreLoading:true, currentPage: (Number(currentPage)+1) }, () => {
                let vars = this.state;
                this.props.getGames(vars);
            });
        }
    }
    searchGames(searchText){
        if (this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
        }
        this.setState({
            searchText:searchText,
            typingTimeout: setTimeout(() => {
                if(searchText.length >= 3){
                    this.setState({getGames:[],searchLoading:true}, () => {
                        let vars = this.state;
                        this.props.getGames(vars);
                    });
                }
                else if(searchText.length == 0){
                    this.setState({getGames:[],searchLoading:false}, () => {
                        this.getData();
                    });
                }
            }, 1000)
        });
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else{
            let data = this.state.getGames;
            let gamesView = [];
            let prizesView = [];
            if(data.length > 0){
                for(let i=0; i<data.length; i++){
                    if(data[i]['prizes'].length > 0){
                        prizesView[i] = [];
                        for(let j=0; j<data[i]['prizes'].length; j++){
                            prizesView[i].push(
                                <List.Item
                                    key={'prize_'+i+'_'+j}
                                    title={data[i]['prizes'][j].diamond+' + '+languages[2][this.state.currentLang]+' $'+data[i]['prizes'][j].prize}
                                    titleStyle={{marginLeft:-32,fontSize:14}}
                                    left={props => <List.Icon {...props} color={'#005d67'} icon={'diamond-stone'} />}
                                    right=  {
                                                props => <Button icon="gamepad" mode="contained" onPress={() => console.log('Pressed')} labelStyle={{color:SECOND_COLOR}}>
                                                            {languages[1][this.state.currentLang]}
                                                        </Button>
                                            }
                                    style={{borderBottomWidth:1,borderBottomColor:'#ddd'}}
                                />
                            );
                        }
                    }
                    gamesView.push(
                        <Card key={'games_'+i} style={{marginBottom:20,marginLeft:20,marginRight:20}}>
                            <Card.Cover source={{ uri: 'https://smokeapps.com/assets/gfx/games/'+(data[i].src_image).replace(".", "_400.") }} />
                            <Card.Content>
                                <Title style={{position:'absolute',marginLeft:10,marginTop:-40}}>
                                    <View style={{backgroundColor:'rgba(0, 0, 0, 0.7)',padding:10,borderRadius:5}}>
                                        <Text style={{color:'#fff'}}>{data[i].game}</Text>
                                    </View>
                                </Title>
                                <View>
                                    {prizesView[i]}
                                </View>
                            </Card.Content>
                        </Card>
                    );
                }
            }
            let bottomLoading;
            if(this.state.moreLoading == true){
                bottomLoading = (
                    <ActivityIndicator />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:SECOND_COLOR}}>
                        {/*<Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />*/}
                        <Image source={logo} style={{resizeMode:'contain',width:Dimensions.get('window').width,height:40}} />
                    </Appbar.Header>
                    <Searchbar
                        placeholder={languages[0][this.state.currentLang]}
                        onChangeText={(text)=>this.searchGames(text)}
                        value={this.state.searchText}
                        style={{margin:15}}
                    />
                    <ScrollView
                        onScroll={(e) => {
                            let paddingToBottom = 100;
                            paddingToBottom += e.nativeEvent.layoutMeasurement.height;
                            if((e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom)) {
                                this.getMoreData();
                            }
                        }}
                        refreshControl={<RefreshControl refreshing={this.state.refresh} onRefresh={()=>this.getData()} />}
                    >
                        {gamesView}
                        {bottomLoading}
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    }
});

function mapStateToProps(state, props) {
    return {
        getGamesRes: state.productReducer.getGamesRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);