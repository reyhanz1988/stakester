import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Appbar, Icon, List } from 'react-native-paper';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Settings';

class SettingsScreen extends React.Component {
    componentDidMount() {
        AsyncStorage.getItem("LANG").then(value => {
        if(value == null){
            AsyncStorage.setItem('LANG', 'en').then((value) => {
                this.setState({currentLang: 'en'}, () =>{
                    this.setState({isLoading:false});
                });
            });
        }
        else{
            this.setState({ currentLang: value }, () =>{
                this.setState({isLoading:false});
            });
        }});
    }
    componentDidUpdate(prevprops) {
        
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentLang: 'en',
        };
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else{
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header>
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <ScrollView style={{marginLeft:10,marginRight:10}}>
                        <List.Item
                            title={languages[1][this.state.currentLang]}
                            left={props => <List.Icon {...props} icon={'account-multiple-plus'} />}
                            right={props => <List.Icon {...props} icon='chevron-right' />}
                            onPress={() => console.log('register pressed')}
                            style={{borderBottomWidth:1,borderBottomColor:'#ddd'}}
                        />
                        <List.Item
                            title={languages[2][this.state.currentLang]}
                            left={props => <List.Icon {...props} icon={'login'} />}
                            right={props => <List.Icon {...props} icon='chevron-right' />}
                            onPress={() => console.log('login pressed')}
                            style={{borderBottomWidth:1,borderBottomColor:'#ddd'}}
                        />
                        <List.Item
                            title={languages[3][this.state.currentLang]}
                            left={props => <List.Icon {...props} icon={'flag'} />}
                            right={props => <List.Icon {...props} icon='chevron-right' />}
                            onPress={() => this.props.navigation.navigate('Languages')}
                            style={{borderBottomWidth:1,borderBottomColor:'#ddd'}}
                        />
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
    }
});

function mapStateToProps(state, props) {
    return {
        getGamesRes: state.productReducer.getGamesRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);