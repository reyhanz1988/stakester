import React from 'react';
import { connect } from 'react-redux';
import * as masterActions from '../actions/masterActions';
import { StyleSheet, ScrollView, View } from 'react-native';
import { Appbar, Icon, List } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CountryFlag from "react-native-country-flag";
import RNRestart from 'react-native-restart';
import languages from '../screensTranslations/Languages';
import Loading from '../components/Loading';
//import Reload from '../components/Reload';
import {FIRST_COLOR} from "@env";

class LanguagesScreen extends React.Component {
    componentDidMount() {
        AsyncStorage.getItem("LANG").then(value => {
        if(value == null){
            AsyncStorage.setItem('LANG', 'en').then((value) => {
                this.setState({currentLang: 'en'}, () =>{
                    this.getData();
                });
            });
        }
        else{
            this.setState({ currentLang: value }, () =>{
                this.getData();
            });
        }});
    }   
    componentDidUpdate(prevprops) {
        if(prevprops.getLangRes != this.props.getLangRes){
            if(this.props.getLangRes == 'error'){
                this.setState({
                    getDataError: this.props.getLangRes,
                    isLoading: false
                });
            }
            else{
                if(this.props.getLangRes.status_msg){
                    this.setState({
                        getDataError: this.props.getLangRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        getLang: this.props.getLangRes ,
                        isLoading:false
                    });
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentLang: 'en',
            currentCurrency:'USD',
            getLang:[],
            getDataError:''
        };
        this.getData = this.getData.bind(this);
        this.saveForm = this.saveForm.bind(this);
    }
    getData(){
        this.setState({getDataError: '',isLoading:true}, () =>{
            this.props.getLang();
        });
    }
    saveForm(lang){
        AsyncStorage.setItem('LANG', lang).then((value) => {
            this.setState({
                currentLang: lang
            }, () =>{
                RNRestart.Restart();
            });
        });
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        /*else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }*/
        else{
            let getLang = this.state.getLang;
            let listViews = [];
            for(let i=0;i<getLang.length;i++){
                let bgColor;
                if(getLang[i].language_iso == this.state.currentLang){
                    bgColor = "#FADA5E";
                }
                else{
                    bgColor = "#fff";
                }
                let cfIcon = getLang[i].language_iso;
                if(getLang[i].language_iso == 'en'){
                    cfIcon = 'gb';
                }
                listViews.push(
                    <List.Item
                        key={i}
                        title={getLang[i].language}
                        titleStyle={{marginLeft:20}}
                        left={props => <CountryFlag style={{marginTop:8}} isoCode={cfIcon} size={24} />}
                        right={props => <List.Icon {...props} icon='chevron-right' />}
                        onPress={() => this.saveForm(getLang[i].language_iso)}
                        style={{borderBottomWidth:1,borderBottomColor:'#eee',backgroundColor:bgColor}}
                    />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => this.props.navigation.goBack()} />
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <ScrollView>
                        {listViews}
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
});

LanguagesScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        getLangRes: state.masterReducer.getLangRes
    }
}
const mapDispatchToProps = {
    ...masterActions
};
export default connect(mapStateToProps, mapDispatchToProps)(LanguagesScreen);