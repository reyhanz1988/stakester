package com.stakester;
import android.os.Bundle;
import com.facebook.react.ReactActivity;

public class MainActivity extends ReactActivity {
  @Override
  protected void onCreate(Bundle savedInstance) {
    // Call this before `super.onCreate` if you are using a custom theme for MainActivity
    // setTheme(R.style.AppTheme);

    super.onCreate(savedInstance);
  }

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "stakester";
  }
}