//npm run test -- -i -u
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { create, act } from 'react-test-renderer';
import { cleanup, fireEvent, render, waitFor } from '@testing-library/react-native';
import HomeScreen from '../screens/HomeScreen';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import store from '../store';
import rootReducer from '../reducers/rootReducer';
import productReducer from '../reducers/productReducer';

jest.spyOn(console, 'warn').mockImplementation(() => {})
jest.spyOn(console, 'error').mockImplementation(() => {})

fetch = jest.fn(() => Promise.resolve());

it('checks if Async Storage is used', async () => {
    await AsyncStorage.setItem('LANG', 'en');
    let asyncLang = await AsyncStorage.getItem('LANG');
    expect(AsyncStorage.getItem).toBeCalledWith('LANG');
})

const tree = create(
    <Provider store={store}>
        <HomeScreen />
    </Provider>
);

describe('HomeScreen',()=>{
    it('HomeScreen should render properly', () => {
        act(() => {
            expect(tree).toMatchSnapshot();
        });
    });
})

describe('productReducer', () => {
    it('productReducer => initial state', () => {
        expect(rootReducer(productReducer, {})).toMatchSnapshot()
    })
    it('productReducer => GET_GAMES', () => {
        act(() => {
            expect(rootReducer(productReducer, {type: 'GET_GAMES'})).toMatchSnapshot()
        });
    })
});