//npm run test -- -i -u
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { create, act } from 'react-test-renderer';
import { cleanup, fireEvent, render, waitFor } from '@testing-library/react-native';
import SettingsScreen from '../screens/SettingsScreen';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import store from '../store';
import rootReducer from '../reducers/rootReducer';

jest.spyOn(console, 'warn').mockImplementation(() => {})
jest.spyOn(console, 'error').mockImplementation(() => {})

fetch = jest.fn(() => Promise.resolve());

it('checks if Async Storage is used', async () => {
    await AsyncStorage.setItem('LANG', 'en');
    let asyncLang = await AsyncStorage.getItem('LANG');
    expect(AsyncStorage.getItem).toBeCalledWith('LANG');
})

const tree = create(
    <Provider store={store}>
        <SettingsScreen />
    </Provider>
);

describe('SettingsScreen',()=>{
    it('SettingsScreen should render properly', () => {
        act(() => {
            expect(tree).toMatchSnapshot();
        });
    });
})