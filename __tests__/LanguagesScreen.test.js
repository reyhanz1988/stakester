//npm run test -- -i -u
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { create, act } from 'react-test-renderer';
import { cleanup, fireEvent, render, waitFor } from '@testing-library/react-native';
import LanguagesScreen from '../screens/LanguagesScreen';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import store from '../store';
import rootReducer from '../reducers/rootReducer';
import masterReducer from '../reducers/masterReducer';

jest.spyOn(console, 'warn').mockImplementation(() => {})
jest.spyOn(console, 'error').mockImplementation(() => {})

fetch = jest.fn(() => Promise.resolve());

it('checks if Async Storage is used', async () => {
    await AsyncStorage.setItem('LANG', 'en');
    let asyncLang = await AsyncStorage.getItem('LANG');
    expect(AsyncStorage.getItem).toBeCalledWith('LANG');
})

const tree = create(
    <Provider store={store}>
        <LanguagesScreen />
    </Provider>
);

describe('LanguagesScreen',()=>{
    it('LanguagesScreen should render properly', () => {
        act(() => {
            expect(tree).toMatchSnapshot();
        });
    });
})

describe('masterReducer', () => {
    it('masterReducer => initial state', () => {
        expect(rootReducer(masterReducer, {})).toMatchSnapshot()
    })
    it('masterReducer => GET_LANG', () => {
        act(() => {
            expect(rootReducer(masterReducer, {type: 'GET_LANG'})).toMatchSnapshot()
        });
    })
});