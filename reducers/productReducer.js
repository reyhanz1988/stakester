import {
    GET_GAMES
} from "../actions/productActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    getGamesRes:[],
};

const productReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_GAMES                                                                                                                               */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_GAMES.REQUESTED:
            return { ...state };
        case GET_GAMES.SUCCESS:
            return {
                ...state,
                getGamesRes: payload.response,
                errorMsg: '',
            };
        case GET_GAMES.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        default:
            return state
    }
}

function cloneObject(object){
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}


export default productReducer