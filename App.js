import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LogBox, Platform, StatusBar, StyleSheet, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import AppNav from './navigation/AppNav';
import RNRestart from 'react-native-restart';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Provider } from 'react-redux';
import store from './store';
import Orientation from 'react-native-orientation-locker';
import {FIRST_COLOR, SECOND_COLOR} from "@env";

LogBox.ignoreLogs(['Warning: ...']); // Ignore log notification by message
LogBox.ignoreAllLogs(); //Ignore all log notifications 

const theme = {
    ...DefaultTheme,
    roundness: 5,
    colors: {
        ...DefaultTheme.colors,
        primary: FIRST_COLOR,
        accent: SECOND_COLOR,
    },
};

export default class App extends React.Component {
    componentDidMount() {
        Orientation.getAutoRotateState((rotationLock) => this.setState({rotationLock}));
        Orientation.lockToPortrait();
        AsyncStorage.getItem("LANG").then(value => {
        if(value == null){
            AsyncStorage.setItem('LANG', 'en').then((value) => {
                this.setState({currentLang: 'en'});
            });
        }
        else{
            this.setState({ currentLang: value });
        }});
    }
    componentDidUpdate(prevprops) {
        
    }
    constructor(props) {
        super(props);
        this.state = {
            rotationLock: '',
            currentLang: 'en',
            isLoading: true,
        };
    }
    render(){
        return(
            <Provider store={store}>
                <View style={styles.container}>
                    {Platform.OS === 'ios' && <StatusBar />}
                    <NavigationContainer>
                        <PaperProvider theme={theme}>
                            <AppNav />
                        </PaperProvider>
                    </NavigationContainer>
                </View>
            </Provider>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});